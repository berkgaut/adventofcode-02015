{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (many1, letter, Parser, parseOnly, string, char, decimal)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Control.Applicative ((<|>))
import Data.List

import Commons

type Seconds = Int
type KmS = Int
type Km = Int

data Reindeer = Reindeer { name :: String, speed :: KmS, move :: Seconds, rest :: Seconds} deriving (Eq, Show)

posAt :: Reindeer -> Seconds -> Km
posAt r s = movementTime * (speed r) where
	cycleTime = (move r + rest r)
	fullCycles = s `div` cycleTime
	restOfTime = (move r) `min` (s - fullCycles * cycleTime)
	movementTime = (fullCycles * (move r)) + restOfTime

reindeerP :: Parser Reindeer
reindeerP = do
	name <- many1 letter
	string " can fly "
	speed <- decimal
	string " km/s for "
	move <- decimal
	string " seconds, but then must rest for "
	rest <- decimal
	string " seconds."
	return $ Reindeer { name=name, speed=speed, move=move, rest=rest }

parseReindeer :: T.Text -> [Reindeer]
parseReindeer t = case parseOnly reindeerP t of
	Left _ -> []
	Right r -> [r]

processInput ::  Seconds -> T.Text -> (Reindeer, Km)
processInput s input = head . reverse . sortOn snd . map (\r -> (r, posAt r s)) . concatMap parseReindeer . T.lines $ input

main :: IO ()
main = do
	let comet = Reindeer {name="Comet", speed=14, move=10, rest=127}
	let dancer = Reindeer {name="Dancer", speed=16, move=11, rest=162}
	(posAt comet, 1) `shouldBe` 14
	(posAt dancer, 1) `shouldBe` 16
	(posAt comet, 10) `shouldBe` 140
	(posAt dancer, 10) `shouldBe` 160
	(posAt comet, 11) `shouldBe` 140
	(posAt dancer, 11) `shouldBe` 176
	(posAt dancer, (11+162)*2) `shouldBe` (16*11*2)
	(parseOnly reindeerP, "Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.") `shouldBe` (Right $ Reindeer { name="Comet", speed=14, move=10, rest=127})

	testInput <- TIO.readFile "14_test.txt"
	(snd . processInput 1000, testInput) `shouldBe` 1120

	input <- TIO.readFile "14.txt"
	putStrLn . show . processInput 2503 $ input




