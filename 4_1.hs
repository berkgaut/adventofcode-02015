import qualified Crypto.Hash.MD5 as MD5
import qualified Data.ByteString as B
import Data.Char (ord)
import Data.Bits ((.&.), shiftR)
import Data.List (find)

md5 :: String -> B.ByteString
md5 s = MD5.hash $ B.pack $ map (fromIntegral . ord) $ s

isMagic maglevel bs = B.all (==0) $ B.take maglevel $ B.concatMap (\w8 -> B.pack [w8 `shiftR` 4, w8 .&. 0x0F]) $ bs

isSolution maglevel secret n = isMagic maglevel $ md5 $ secret ++ (show n)

solve maglevel secret = find (isSolution maglevel secret) $ [1,2..]

solve1 = solve 5

solve2 = solve 6 -- could be optimized by looking for just first 3 bytes are zero

