import Data.Digits -- from digits
import Data.Char (ord, chr)
import Data.List

digit_to_char d = chr $ d + ord 'a'

char_to_digit c = (ord c) - (ord 'a')

n_to_string n = map digit_to_char $ digits 26 n

string_to_n s = unDigits 26 $ map char_to_digit s

check1 [] = False
check1 [_] = False
check1 [_,_] = False
check1 (x:y:z:rest) = ((x `s` y) && (y `s` z)) || (check1 (y:z:rest)) where
  s c1 c2 = (ord c1) + 1 == (ord c2)

check2 s = Nothing == find prohibited s where
  prohibited c = elem c "iol"

check3 s =  (>=2) . length . filter ((==2).length) $ group s

check s = check1 s && check2 s && check3 s

nextSatisfying s = head $ filter check $ map n_to_string $ [string_to_n s + 1 .. ]

main = do
  let result1 = nextSatisfying "hepxxyzz"
  putStrLn result1
  putStrLn $ nextSatisfying result1