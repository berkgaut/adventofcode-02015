import Data.List

-- this solution is remarkably compact, thanks to powerful group primitive

step = intercalate [] . map (\g -> (show . length $ g) ++ [head g]) . group

-- tried 41 first but it's wrong)
part1 = putStrLn $ show $ length $ iterations !! 40 where
  iterations = iterate step "1321131112"

part2 = putStrLn $ show $ length $ iterations !! 50 where
  iterations = iterate step "1321131112"

