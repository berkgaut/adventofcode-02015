import Data.List

part1 = do
  input <- readFile "5.txt"
  let (nice, naughty) = partition isNice $ lines $ input
  putStrLn $ show $ length $ nice

isNice s = cond1 && cond2 && cond3 where
  cond1 = (>=3) $ length $ filter (\c -> c `elem` "aeiou") $ s -- fixed! was >3
  cond2 = any ((>1).length) $ group $ s
  cond3 = not $ any (\l -> any (isPrefixOf l) $ tails s) $ ["ab", "cd", "pq", "xy"]
