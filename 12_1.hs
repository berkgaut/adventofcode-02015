{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import qualified Data.ByteString.Lazy as BS
import qualified Data.Vector as V
import qualified Data.HashMap.Strict as HM
import Control.Monad.State
import Data.Scientific

traverse1 :: Value -> State Scientific ()
traverse1 (Number n) = modify (+n)
traverse1 (Array v) = V.forM_ v traverse1
traverse1 (Object hm) = forM_ (HM.toList hm) (traverse1.snd)
traverse1 _ = return ()

tr f v = snd $ runState (f v) 0

part1 = do
  contents <- BS.readFile "12.json"
  case decode contents of
    (Just v) -> do
      putStrLn $ show $ tr traverse1 v
      putStrLn $ show $ tr traverse2 v
    Nothing -> putStrLn "error parsing json"


traverse2 :: Value -> State Scientific ()
traverse2 (Number n) = modify (+n)
traverse2 (Array v) = V.forM_ v traverse2 -- bug during copypaste: forgot to replace with traverse1. Morale: separate recursion schemes from value code
traverse2 (Object hm) = unless markedRed $ forM_ ll (traverse2.snd) where
  ll = HM.toList hm
  markedRed = any (\ (_,v)->v=="red") ll
traverse2 _ = return ()
