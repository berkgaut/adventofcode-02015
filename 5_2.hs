import Data.List

part2 = do
  input <- readFile "5.txt"
  let (nice, naughty) = partition isNice $ lines $ input
  putStrLn $ show $ length $ nice

isNice s = cond1 s && cond2 s

sarr s = sortOn fst $ zip (tails s) [1,2..]

sarr2 s = filter ((>=2).length.fst) $ sarr s

cond1 s = any nonOverlaping $ filter ((>=2).length) $ groupBy first2 $ sarr2 $ s where
  first2 (a1:b1:_,_) (a2:b2:_,_) = a1 == a2 && b1 == b2
  nonOverlaping listOfSuffixes = maxIndex - minIndex >= 2 where
    minIndex = minimum indices
    maxIndex = maximum indices
    indices = map snd listOfSuffixes

cond2 [] = False
cond2 [_,_] = False
cond2 (a:b:c:rest) = a == c || cond2 (b:c:rest)