import Data.List
import Data.Char
import System.IO

type D = Int

parseLine :: String -> (D, D, D)
parseLine line = (read w, read h, read l) where
  (w, 'x':r1) = span isDigit line
  (h, 'x':r2) = span isDigit r1
  (l, []) = span isDigit r2

parseInput = map parseLine . lines

area (w,h,l) = wrap + slack where
  wrap = 2 * s1 + 2 * s2 + 2 * s3
  slack = min s1 $ min s2 s3 -- could be expressed as foldl1 min [s1,s2,s3]
  s1=w*h
  s2=w*l
  s3=h*l

ribbon (w,h,l) = wrap + bow where
  wrap = min p1 $ min p2 p3
  bow = w*h*l
  p1 = 2*(w+h) -- fixed, initially was w*h; case for using dimentional control!
  p2 = 2*(w+l)
  p3 = 2*(h+l)

part1 = do
  input <- readFile "2_1.txt"
  putStrLn $ show . sum . map area . parseInput $ input

part2 = do
  input <- readFile "2_1.txt"
  putStrLn $ show . sum . map ribbon . parseInput $ input
