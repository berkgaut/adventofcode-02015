{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text
import qualified Data.Text as T
import Control.Applicative ((<|>))
import Data.List
import qualified Data.Map as M
import qualified Data.Set as S

data Dist = Dist String String Int deriving Show

parseDist :: Parser Dist
parseDist = do
  from <- many' (notChar ' ')
  string " to "
  to   <- many' (notChar ' ')
  string " = "
  dist <- decimal
  return $ Dist from to dist

parseD s = case parseOnly parseDist s of
  Right d -> d

distMap l = foldr g M.empty l where
  g (Dist from to dist) m = M.insert (from, to) dist $ M.insert (to, from) dist m

locations l = S.toList $ foldr g S.empty l where
  g (Dist from to _) s = S.insert from $ S.insert to s

routeLength r m = fst $ foldr g (0, head r) (tail r) where
  g x (acc, cur) = (acc + dist, x) where
    dist = (M.!) m (cur,x)

part1 = do
  input <- readFile "9.txt"
  let inputData = map (parseD . T.pack) $ lines $ input
  let dmap = distMap inputData
  let allLocations = locations inputData
  let allRoutesAndLengthes = sortOn snd $ map (\r->(r, routeLength r dmap)) (permutations allLocations)
  putStrLn $ show $ head $  allRoutesAndLengthes
  putStrLn $ show $ head $ reverse $ allRoutesAndLengthes