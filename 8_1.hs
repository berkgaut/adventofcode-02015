{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text
import qualified Data.Text as T
import Control.Applicative ((<|>))
import Data.List

parseStringLiteral :: Parser String
parseStringLiteral = do
  char '"'
  l <- many' parseChar
  char '"'
  return l

parseChar =     (char '\\' >> char '\\' >> return '\\')
            <|> (char '\\' >> char '"' >> return '"')
            <|> (do
                   char '\\'
                   char 'x'
                   u <- anyChar
                   w <- anyChar
                   return $ read $ ['\'', '\\', 'x', u, w, '\''])
            <|> notChar '"'

p s = case (parseOnly parseStringLiteral (T.pack s)) of
  Right x -> x
  Left e -> ""

part1 = do
  input <- readFile "8.txt"
  let ll = lines $ input
  let reprSize = sum $ map length $ ll
  let realSize = sum $ map (length . p) $ ll
  putStrLn $ "repr=" ++ (show reprSize) ++ " real=" ++ (show realSize) ++ " diff=" ++ (show $ reprSize - realSize)

encode s = ['"'] ++ (intercalate [] . map encode1 $ s) ++ ['"'] where
  encode1 c = case c of
    '\\' -> ['\\', '\\']
    '"'  -> ['\\', '"']
    x    -> [x]

part2 = do
  input <- readFile "8.txt"
  let ll = lines $ input
  let linesSize = sum $ map length $ ll
  let encodedSize = sum $ map (length.encode) $ ll
  putStrLn $ "orig=" ++ (show linesSize) ++ " enc=" ++ (show encodedSize) ++ " diff=" ++ (show $ encodedSize - linesSize)


