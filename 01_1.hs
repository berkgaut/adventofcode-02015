-- TODO: add problem statement here

problem_1_1 :: String -> Int
problem_1_1 input = foldl f 0 input where
  f acc char = case char of
    '(' -> acc + 1
    ')' -> acc - 1

p input = foldl p' (0,1,[]) input where
  p' (level, position, trace) char = (level', position', (level',position):trace) where
    level' = case char of
      ')' -> level - 1
      '(' -> level + 1
    position' = position + 1

problem1_2 stopLevel input = stopPosition where
  (_, stopPosition) = head t'
  t' = dropWhile (\(level, _) -> level /= stopLevel) (reverse t)
  (_,_,t) = p input

main :: IO ()
main = do
  input <- readFile "01.txt"

  putStrLn "Problem 1.1: "
  putStrLn . show . problem_1_1 $ input

-- TODO: I do not remember what stopLevel should be for second part of problem
--  putStrLn "Problem 1.2: "
--  putStrLn . show . promblem_1_2 $ input
