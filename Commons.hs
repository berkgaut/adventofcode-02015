module Commons 
(
    shouldBe
  , between
)
where

shouldBe :: (Show result, Eq result, Show input) => (input -> result, input) -> result -> IO ()
shouldBe (f, input) expected = let actual = f input in if (actual == expected)
  then putStrLn ("result for '" ++ (show input) ++ "' matches expected '" ++ (show actual) ++ "'")
  else putStrLn ("result for '" ++ (show input) ++ "' is '" ++ (show actual) ++ "', does not match expected '" ++ (show expected) ++ "'") 

between :: (Ord a) => a -> a -> a -> a
between low high x = let bx = if x < low then low else x in if bx > high then high else bx

