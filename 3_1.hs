import Data.List

step (x,y) c = case c of
  '<' -> (x-1,y)
  '>' -> (x+1,y)
  '^' -> (x,y+1)
  'v' -> (x,y-1)
  _   -> (x,y)

trace = scanl step (0,0)

part1 = do
  input <- readFile "3.txt"
  putStrLn . show . length . nub . trace $ input

data Turn = Santa | RoboSanta deriving Eq

teamwork input = (trace santaInput) ++ (trace roboInput) where
  all = zip input (cycle $ [Santa, RoboSanta])
  santaInput = map fst $ filter ((==Santa).snd)     $ all
  roboInput  = map fst $ filter ((==RoboSanta).snd) $ all

part2 = do
  input <- readFile "3.txt"
  putStrLn . show . length . nub . teamwork $ input

