{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text (many1, letter, Parser, parseOnly, string, char, decimal)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Control.Applicative ((<|>))
import Data.List

import Commons

data Fact = SittingBy { name1, name2 :: String, hp :: Int } deriving (Eq, Show)

lookupFact :: [Fact] -> String -> String -> Fact
lookupFact facts n1 n2 = case (find (\f->name1 f == n1 && name2 f == n2) facts) of
		Just x -> x
		-- yes, I want to fail when fact is not found!

allNames :: [Fact] -> [String]
allNames facts = map head . group . sort . map name1 $ facts

happiness :: [Fact] -> [String] -> Int
happiness facts names = sum . map (\[a,b]->(hp $ lookupFact facts a b) + (hp $ lookupFact facts b a)) $ allPairs names where -- потому что нужно смотреть прямой и обратный факты

allPairs :: [String] -> [[String]]
allPairs names = map (\l -> if length l == 2 then l else [head l, head names]) . map (take 2) . filter ((>0).length) . tails $ names

maxHappiness :: [Fact] -> (Int, [String])
maxHappiness facts = head . reverse . sortOn fst . map (\names -> (happiness facts names, names)) . permutations . allNames $ facts

factP :: Parser Fact
factP = do
	name1 <- many1 letter
	string " would "
	sign <- (string "gain" <|> string "lose")
	char ' '
	amount <- decimal
	string " happiness units by sitting next to "
	name2 <- many1 letter
	char '.'
	return $ SittingBy { name1 = name1, name2 = name2, hp = amount * (if sign == "gain" then 1 else -1) }

parseFacts :: T.Text -> [Fact]
parseFacts t = concatMap parseFact . T.lines $ t where
	parseFact l = case (parseOnly factP l) of
		Left _ -> []
		Right fact -> [fact]

processInput :: T.Text -> (Int, [String])
processInput = maxHappiness . parseFacts

addYourself :: [Fact] -> [Fact]
addYourself facts = facts ++ f1 ++ f2 where
	f1 = map (\name -> SittingBy name me 0) all
	f2 = map (\name -> SittingBy me name 0) all
	all = allNames facts
	me = "Me"

processInput2 :: T.Text -> (Int, [String])
processInput2 = maxHappiness . addYourself . parseFacts

main :: IO ()
main = do
	(parseFacts, "Alice would gain 2 happiness units by sitting next to Bob.") `shouldBe` [SittingBy {name1="Alice", name2="Bob", hp=2}]
	(allPairs, ["A", "B", "C"]) `shouldBe` [["A","B"], ["B","C"], ["C", "A"]]

	testInput <- TIO.readFile "13_test.txt"
	putStrLn . show . processInput $ testInput
	(fst . processInput, testInput) `shouldBe` 330

	input <- TIO.readFile "13.txt"
	putStrLn . show . processInput $ input
	putStrLn . show . processInput2 $ input
