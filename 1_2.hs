p input = foldl p' (0,1,[]) input where
  p' (level, position, trace) char = (level', position', (level',position):trace) where
    level' = case char of
      ')' -> level - 1
      '(' -> level + 1
    position' = position + 1

problem1_2 stopLevel input = stopPosition where
  (_, stopPosition) = head t'
  t' = dropWhile (\(level, _) -> level /= stopLevel) (reverse t)
  (_,_,t) = p input

