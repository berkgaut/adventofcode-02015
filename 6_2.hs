{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text
import qualified Data.Text as T
import Control.Applicative ((<|>))
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as M
import qualified Data.Vector.Unboxed.Mutable as UM
import Control.Monad.ST
import Control.Monad

data Op = On | Off | Toggle deriving Show

type Coord = Int

data Instruction = Instruction { op :: Op, x0, y0, x1, y1 :: Coord} | Invalid deriving Show

parseOp :: Parser Op
parseOp =      (string "turn on"  >> return On)
          <|>  (string "turn off" >> return Off)
          <|>  (string "toggle"   >> return Toggle)

parseInstruction :: Parser Instruction
parseInstruction = do
  _op <- parseOp
  space
  _x0 <- decimal
  char ','
  _y0 <- decimal
  string " through "
  _x1 <- decimal
  char ','
  _y1 <- decimal
  return $ Instruction {op=_op, x0=_x0, y0=_y0, x1=_x1, y1=_y1 }

parseI t = case parseOnly parseInstruction t of
  (Right i) -> i
  (Left _)  -> Invalid

part2 = do
  input <- readFile "6.txt"
  let instructions = map (parseI . T.pack) $ lines $ input
  let screen = newScreen 1000 1000
  let screen' = foldl (\scr i -> execInstruction i scr) screen instructions
  putStrLn $ show $ countLights $ screen'

type Screen = V.Vector (U.Vector Int)

newScreen w h = V.replicate h $ U.replicate w $ 0

execOp :: Op -> Int -> Int
execOp On     x = x + 1
execOp Off    x = if x > 0 then x - 1 else 0
execOp Toggle x = x + 2

execInstruction :: Instruction -> Screen -> Screen
execInstruction (Instruction op x0 y0 x1 y1) screen = runST $ do
  mscreen <- V.thaw screen
  forM_ [y0 .. y1] $ \y -> do
    row <- M.read mscreen y
    mrow <- U.thaw row
    forM_ [x0 .. x1] $ \x -> do
      v <- UM.read mrow x
      UM.write mrow x (execOp op v)
    row' <- U.freeze mrow
    M.write mscreen y row'
  V.freeze mscreen

countLights :: Screen -> Int
countLights = V.foldl (U.foldl (+)) 0
