{-# LANGUAGE OverloadedStrings #-}

import Data.Attoparsec.Text
import qualified Data.Text as T
import Control.Applicative ((<|>))
import qualified Data.Map as Map
import Data.Bits
import Debug.Trace
import Control.Monad.State

type WID = String

data Src = Wire WID | Const Int deriving Show

data Form =   Assign {                      src  :: Src,              dst :: WID }
            | Binary { bin_gate :: BinGate, src1 :: Src, src2 :: Src, dst :: WID }
            | Unary  { uni_gate :: UniGate, src  :: Src,              dst :: WID }
            | Invalid
            deriving Show

data BinGate = AND | OR | LSHIFT | RSHIFT deriving Show

data UniGate = NOT deriving Show

parseSrc :: Parser Src
parseSrc =     (many1 letter >>= return . Wire)
           <|> (decimal      >>= return . Const)

parseDst :: Parser WID
parseDst = many1 letter >>= return

mspace = many' space
arrow = string "->"

parseAssign = do
  src <- parseSrc
  mspace
  arrow
  mspace
  dst <- parseDst
  return $ Assign src dst

parseUniGate :: Parser UniGate
parseUniGate = (string "NOT" >> return NOT)

parseUnary = do
  gate <- parseUniGate
  mspace
  src <- parseSrc
  mspace
  arrow
  mspace
  dst <- parseDst
  return $ Unary gate src dst

parseBinGate :: Parser BinGate
parseBinGate =     (string "AND"     >> return AND)
               <|> (string "OR"      >> return OR)
               <|> (string "LSHIFT"  >> return LSHIFT)
               <|> (string "RSHIFT"  >> return RSHIFT)

parseBinary = do
  src1 <- parseSrc
  mspace
  gate <- parseBinGate
  mspace
  src2 <- parseSrc
  mspace
  arrow
  mspace
  dst <- parseDst
  return $ Binary gate src1 src2 dst

parseForm :: Parser Form
parseForm = parseUnary <|> parseAssign <|> parseBinary

parseI t = case parseOnly parseForm t of
  (Right i) -> i
  (Left _)  -> Invalid

-- тут неплохо обдумать, почему вариант без кеширования считался НАСТОЛЬКО дольше. Какая там сложность вообще?

-- evalSrc (Wire wid) m = eval ((Map.!) m wid) m
-- evalSrc (Const c) _  = c

-- eval (Assign src _) m = evalSrc src m
-- eval (Unary NOT src _) m = (.&.0xFFFF) . complement $ s where s = evalSrc src m
-- eval (Binary AND src1 src2 _) m = s1 .&. s2 where
--   s1 = evalSrc src1 m
--   s2 = evalSrc src2 m
-- eval (Binary OR src1 src2 _) m = s1 .|. s2 where
--   s1 = evalSrc src1 m
--   s2 = evalSrc src2 m
-- eval (Binary LSHIFT src1 src2 _) m = (.&.0xFFFF) $ x `shiftL` n where
--   x = evalSrc src1 m
--   n = evalSrc src2 m
-- eval (Binary RSHIFT src1 src2 _) m = (.&.0xFFFF) $ x `shiftR` n where
--   x = evalSrc src1 m
--   n = evalSrc src2 m

-- part0 = do
--   input <- readFile "7a.txt"
--   let instructions = map (parseI . T.pack) $ lines $ input
--   putStrLn $ show $ instructions
--   let insmap = Map.fromList $ map (\f->(dst f, f)) instructions
--   let evmap  = Map.map ((flip eval) insmap) insmap
--   putStrLn $ show $ evmap
--   let h = evalSrc (Wire "h") insmap
--   putStrLn $ show $ h

-- part1 = do
--   input <- readFile "7.txt"
--   let instructions = map (parseI . T.pack) $ lines $ input
--   let insmap = Map.fromList $ map (\f->(dst f, f)) instructions
--   let a = evalSrc (Wire "a") insmap
--   putStrLn $ show $ a

data ES = ES { estate :: Map.Map WID Int, edefs :: Map.Map WID Form }

evalSrc :: Src -> State ES Int
evalSrc src = case src of
    (Const i) -> return i
    (Wire wid) -> do
      st <- get
      case (Map.lookup wid (estate st)) of
        (Just v) -> return v
        Nothing -> do
          let def = (Map.!) (edefs st) wid
          result <- evalDef def
          modify $ \s -> s{estate = Map.insert wid result (estate s)} -- (!!!) вот это стоило нескольких дней, потому что я брал сначала st, в которой не отражены результаты evalDef и кеширование не работало. Вывод: работа с протягиванием состояния в голом виде очень опасна. в идеале нужно запретить put и пользоваться только get для чтения и modify для записи
          return  result

evalDef :: Form -> State ES Int
evalDef form = case form of
  (Assign src _) -> do
    result <- evalSrc src
    return result
  (Binary gate src1 src2 _) -> do
    a1 <- evalSrc src1
    a2 <- evalSrc src2
    return $ evalBin gate a1 a2
  (Unary gate src _) -> do
    a <- evalSrc src
    return $ evalUni gate a

evalAll :: State ES ()
evalAll = do
  st <- get
  let allK = Map.keys $ edefs $ st
  forM_ allK $ \wid -> do
    evalSrc (Wire wid)

evalBin AND a b = a .&. b
evalBin OR  a b = a .|. b
evalBin LSHIFT x n = (.&.0xFFFF) $ x `shiftL` n
evalBin RSHIFT x n = (.&.0xFFFF) $ x `shiftR` n

evalUni NOT a = (.&.0xFFFF) . complement $ a

part0 = do
  input <- readFile "7a.txt"
  let instructions = map (parseI . T.pack) $ lines $ input
  putStrLn $ show $ instructions
  let insmap = Map.fromList $ map (\f->(dst f, f)) instructions
  let es = ES Map.empty insmap
  let (_, st) = runState evalAll es
  putStrLn $ show $ estate $ st

main = do
  input <- readFile "7.txt"
  let instructions = map (parseI . T.pack) $ lines $ input
  let insmap = Map.fromList $ map (\f->(dst f, f)) instructions
  let es = ES Map.empty insmap
  let (a, _) = runState (evalSrc (Wire "a")) es
  putStrLn $ "part 1 = " ++ (show a)
  let insmap' = Map.insert "b" (Assign (Const a) "b") insmap
  let es' = ES Map.empty insmap'
  let (a', _) = runState (evalSrc (Wire "a")) es'
  putStrLn $ "part 2 = " ++ (show a')
